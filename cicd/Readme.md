## Cloud Run Deployment

We are using Google Cloud Run , Github Action, Cloud Build & Terraform for the deployment process. Following are the responsibilities defined within the services -:

1) [Cloud Run](https://cloud.google.com/run) -: Serverless platform made by Google to host container images , it is the final place where code will run.
2) [Cloud Build](https://cloud.google.com/cloud-build) -: Which will build our current commit as a docker image on every push where the docker image will be stored on google which we will require in cloud run(as it only accepts image).
3) [Terraform](https://www.terraform.io/) -: Terraform creates , updates & destroys Cloud Run Instance. Workspace is one of the features of Terraform where one can create multiple environments. For Cloud Run since every image has different urls so on changing the container image while applying terraform it will auto build a new version of Cloud Run. It is very easy working in cloud run since most of the parts are already handled by Terraform & Google. 
4) [Github Action](https://docs.github.com/en/free-pro-team@latest/actions) -: It will trigger a bash script , which will determine which branch to be deployed on which instance (environment test / staging & live).   




#### Installation process 

- Make a project in Google Cloud
- Make a Github repo & subscribe to every build using Google Cloud Build (make sure you create trigger for every branch push) 
- Clone this `cicd` folder in to your repo's root
- Make necessary changes on `cicd/dev.tfvars` and `cicd/credentials/dev-cred.json` as it it only for single enviornment
- Copy github actions i.e (`.github/workflows/deploy.yml`) into your repo which will deploy your image once it is being triggered using 
[Repository Dispatch](https://docs.github.com/en/free-pro-team@latest/actions/reference/events-that-trigger-workflows#repository_dispatch)
- `cicd/trigger-deploy` is a shell script where our trigger lies, it uses your [Github Personal Token](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token)
where one can deploy using `./cicd/trigger-deploy staging master`


### Steps for creating multiple environment
- Copy `cicd/dev.tfvars` with your `cicd/test.tfvars` or `cicd/live.tfvars` and replace credentials with the one you need.
- Inside `cicd/deploy.go` there are some checks which decides which env to load on depending upon the variables like `trigger-deploy ${environment} ${branch}` so ad new checks according to your environment
- Once you make changes to `deploy.go` make sure you run `go build deploy.go` as Github Actions uses final binary file to execute 



Disclaimer -: Since the docs does not cover about how to run Cloud Run, Terraform & Github Action. Assuming you have already worked with these things before.


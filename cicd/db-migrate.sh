set -eu

if [ -z "$1" ]
  then
    echo "No environment specified"
fi

wget https://github.com/GoogleCloudPlatform/docker-credential-gcr/releases/download/v2.0.2/docker-credential-gcr_linux_amd64-2.0.2.tar.gz
tar -xvzf docker-credential-gcr_linux_amd64*
sudo mv ./docker-credential-gcr /usr/local/bin/
sudo docker-credential-gcr configure-docker --registries="gcr.io"
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R
export GOOGLE_APPLICATION_CREDENTIALS=$(pwd)/credentials/$1-cred.json

docker network create typeorm-cloudsqlproxy
echo $APP_IMAGE
docker run -d --mount type="bind,source=$(pwd)/credentials/$1-cred.json,target=/config.json"  \
  --name=cloudsql-proxy \
  --network typeorm-cloudsqlproxy \
  -p "127.0.0.1:$DATABASE_PORT:$DATABASE_PORT" \
  gcr.io/cloudsql-docker/gce-proxy:1.12 /cloud_sql_proxy \
  "-instances=$DATABASE_INSTANCE=tcp:0.0.0.0:$DATABASE_PORT" -credential_file=/config.json

docker run \
  --rm \
  --network typeorm-cloudsqlproxy \
  -e "DATABASE_HOST=cloudsql-proxy" \
  -e "DATABASE_PORT=$DATABASE_PORT" \
  -e "DATABASE_NAME=$DATABASE_NAME" \
  -e "DATABASE_USER=$DATABASE_USER" \
  -e "DATABASE_PASSWORD=$DATABASE_PASSWORD" \
  $APP_IMAGE \
  npm run typeorm:prod -- migration:run

docker stop cloudsql-proxy

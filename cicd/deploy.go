package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func main() {
	env := os.Args[1]
	credentialFilePath := getCredentialsFilePath(string(env))
	commitHash := getCommitHash()
	os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", credentialFilePath)
	initTerraform()
	applyTerraform(commitHash, string(env))
	fmt.Println("deployed successfully")
}

func applyTerraform(commitHash string, env string) {
	cmd := exec.Command("terraform", "apply", "-var", "image_tag="+commitHash, "-var-file="+getTfVarFileName(env), "-auto-approve", "-lock=false")

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}

func getCommitHash() string {
	commitHash, err := exec.Command("git", "rev-parse", "HEAD").Output()

	if err != nil {
		os.Exit(1)
	}

	return strings.TrimSuffix(string(commitHash), "\n")
}

func initTerraform() {
	cmd := exec.Command("terraform",
		"init",
		"-backend-config",
		"bucket=firebaseapi-1",
	)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}

func getTfVarFileName(env string) string {

	if env == "dev" {
		return "dev.tfvars"
	}

	if env == "production" {
		return "prof.tfvars"
	}
	panic("Please select correct environment only staging & production available at the moment")
}

func getCredentialsFilePath(env string) string {

	if env == "dev" {
		return "credentials/dev-cred.json"
	}

	if env == "production" {
		return "credentials/prod-cred.json"
	}

	panic("error on loading credentials")
}

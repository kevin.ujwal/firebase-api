package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

type EnvironmentConfig struct {
	environment          string
	project              string
	machine_type         string
	region               string
	credential_file_path string
	zone                 string
	app_name             string
	github_repo_name     string
}

func main() {
	env := os.Args[1]
	credentialFilePath := getCredentialsFilePath(string(env))
	configJson := convertTfConfigToJson(getTfVarFileName(string(env)))
	commitHash := getCommitHash()
	os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", credentialFilePath)
	dockerLogin(credentialFilePath)
	dockerBuild(configJson, commitHash)
	fmt.Println("build successfully")
}

func convertTfConfigToJson(fileName string) EnvironmentConfig {

	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	jsonMap := map[string]interface{}{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), "=")
		value := s[1]
		jsonMap[s[0]] = strings.TrimSuffix(value[1:len(value)-1], "\n") // this will remove quote & breakline
	}

	fmt.Println(jsonMap["app_name"])
	if scanner.Err() != nil {
		log.Fatal(err)
	}

	return EnvironmentConfig{
		environment:          jsonMap["environment"].(string),
		project:              jsonMap["project"].(string),
		machine_type:         jsonMap["machine_type"].(string),
		region:               jsonMap["region"].(string),
		credential_file_path: jsonMap["credential_file_path"].(string),
		zone:                 jsonMap["zone"].(string),
		app_name:             jsonMap["app_name"].(string),
		github_repo_name:     jsonMap["github_repo_name"].(string),
	}
}

func pullExistingImage(imageUrl string) error {
	p := exec.Command(
		"docker",
		"pull",
		imageUrl,
	)
	p.Stdout = os.Stdout
	p.Stderr = os.Stderr
	err := p.Run()

	return err

}

func dockerBuild(config EnvironmentConfig, commitHash string) {

	fullGcrUrl := fmt.Sprintf("gcr.io/%s/%s:%s", config.project, config.github_repo_name, commitHash)
	localBuiltImagePath := fmt.Sprintf("%s:%s", config.github_repo_name, commitHash)
	pullImage := pullExistingImage(fullGcrUrl)

	if pullImage != nil {
		buildImage := exec.Command(
			"docker",
			"build",
			"-t",
			localBuiltImagePath,
			"../",
			"--target",
			"production",
		)

		buildImage.Stdout = os.Stdout
		buildImage.Stderr = os.Stderr
		buildImage.Run()

		createDockerImageTag(localBuiltImagePath, fullGcrUrl)
		pushDockerImage(fullGcrUrl)
	}
}

func pushDockerImage(fullGcrUrl string) {
	push := exec.Command(
		"docker",
		"push",
		fullGcrUrl,
	)
	push.Stdout = os.Stdout
	push.Stderr = os.Stderr
	push.Run()
}

func createDockerImageTag(localBuiltImagePath string, fullGcrUrl string) {
	tag := exec.Command(
		"docker",
		"tag",
		localBuiltImagePath,
		fullGcrUrl,
	)
	tag.Stdout = os.Stdout
	tag.Stderr = os.Stderr
	tag.Run()
}

func dockerLogin(filePath string) {
	p := exec.Command(
		"docker",
		"login",
		"-u",
		"_json_key",
		"--password-stdin",
		"https://gcr.io",
	)

	file, _ := ioutil.ReadFile(filePath)
	p.Stdin = strings.NewReader(string(file))
	p.Stdout = os.Stdout
	p.Stderr = os.Stderr
	err := p.Run()
	if err != nil {
		fmt.Println("erro")
	}
}

func getCommitHash() string {
	commitHash, err := exec.Command("git", "rev-parse", "HEAD").Output()

	if err != nil {
		os.Exit(1)
	}

	return strings.TrimSuffix(string(commitHash), "\n")
}

func getTfVarFileName(env string) string {

	if env == "dev" {
		return "dev.tfvars"
	}

	if env == "production" {
		return "prof.tfvars"
	}
	panic("Please select correct environment only staging & production available at the moment")
}

func getCredentialsFilePath(env string) string {

	if env == "dev" {
		return "credentials/dev-cred.json"
	}

	if env == "production" {
		return "credentials/prod-cred.json"
	}

	panic("error on loading credentials")
}

provider "google" {
  credentials = "${file(var.credential_file_path)}"
  project     = var.project
  region      = var.region
  zone        = var.zone
  version     = "~> 3.0.0"
}

provider "google-beta" {
  credentials = "${file(var.credential_file_path)}"
  project     = var.project
  region      = var.region
  version     = "~> 3.43"
}

variable "image_tag" {
  type = string
}

terraform {
  backend "gcs" {
    prefix="api"
  }
}

locals {
  app_image = "gcr.io/${var.project}/${var.github_repo_name}:${var.image_tag}"
}


resource "google_cloud_run_service" "api" {
  name                       = var.app_name
  location                   = "us-central1"
  provider                   = "google-beta"
  autogenerate_revision_name = true
  template {
    spec {
      containers {
        image = local.app_image

        env {
          name = "API_KEY"
          value = "AIzaSyDf1EflhyxFilEn4_hzKstuXz0TsOadVS4"
        }

        env {
          name = "AUTH_DOMAIN"
          value = "https://react-table-3c63b.web.app/"
        }
        env {
          name = "BUCKET"
          value = "react-table-3c63b-default-rtdb"
        }

        env {
          name = "DATABASE_URL"
          value = "https://react-table-3c63b-default-rtdb.firebaseio.com/"
        }

      }
    }

      metadata {
          annotations = {
            "autoscaling.knative.dev/maxScale"      = "1000"
            "run.googleapis.com/client-name"        = "terraform"
          }
        }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}



data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}
resource "google_cloud_run_service_iam_policy" "noauth" {
  location = "us-central1"
  project  = var.project
  provider = "google-beta"
  service  = google_cloud_run_service.api.name

  policy_data = data.google_iam_policy.noauth.policy_data
}
